-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Фев 13 2019 г., 02:22
-- Версия сервера: 10.1.30-MariaDB
-- Версия PHP: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `bsd_archive`
--

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_freedoc`
--

CREATE TABLE `tbl_freedoc` (
  `id_doc` int(11) NOT NULL,
  `id_user` int(3) NOT NULL,
  `stage` int(1) NOT NULL DEFAULT '1',
  `cabinet` int(1) NOT NULL,
  `size_mc` float NOT NULL DEFAULT '0',
  `size_wc` float NOT NULL DEFAULT '0',
  `package` int(5) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_list`
--

CREATE TABLE `tbl_list` (
  `id_doc` int(11) NOT NULL,
  `id_user` int(3) NOT NULL,
  `date_proc` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `count` int(4) NOT NULL,
  `stage` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_name`
--

CREATE TABLE `tbl_name` (
  `id_doc` int(11) NOT NULL,
  `rubrika` varchar(8) NOT NULL,
  `fond` varchar(10) NOT NULL,
  `opis` varchar(10) NOT NULL,
  `delo` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_registrate`
--

CREATE TABLE `tbl_registrate` (
  `id_user` int(3) NOT NULL,
  `date_in` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_out` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_registrate`
--

INSERT INTO `tbl_registrate` (`id_user`, `date_in`, `date_out`) VALUES
(1, '2019-02-12 23:19:46', '2019-02-13 01:19:46'),
(1, '2019-02-12 23:21:28', '2019-02-13 01:21:28');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_reverse`
--

CREATE TABLE `tbl_reverse` (
  `sender` int(3) NOT NULL,
  `catcher` int(3) NOT NULL,
  `date_return` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_doc` int(11) NOT NULL,
  `text` varchar(200) NOT NULL,
  `complete` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_specialize`
--

CREATE TABLE `tbl_specialize` (
  `specialize` varchar(15) NOT NULL,
  `short_name` varchar(10) DEFAULT NULL,
  `id_special` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_specialize`
--

INSERT INTO `tbl_specialize` (`specialize`, `short_name`, `id_special`) VALUES
('Сканировщик', 'scan', 1),
('Обработчик', 'proc', 2),
('Проверяющий', 'check', 3),
('Администратор', 'adm', 5);

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_stage`
--

CREATE TABLE `tbl_stage` (
  `stage` int(1) NOT NULL,
  `stage_name` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_stage`
--

INSERT INTO `tbl_stage` (`stage`, `stage_name`) VALUES
(0, 'ожидание'),
(1, 'сканирование'),
(2, 'обработка'),
(3, 'проверка'),
(4, 'выполнено'),
(6, 'чёрный список');

-- --------------------------------------------------------

--
-- Структура таблицы `tbl_user`
--

CREATE TABLE `tbl_user` (
  `name` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `fathername` varchar(30) NOT NULL,
  `id_user` int(3) NOT NULL,
  `shift` int(1) NOT NULL,
  `cabinet` int(1) NOT NULL,
  `id_special` int(1) NOT NULL,
  `block` tinyint(1) NOT NULL DEFAULT '0',
  `password` varchar(20) NOT NULL DEFAULT '11'
) ENGINE=InnoDB DEFAULT CHARSET=cp1251;

--
-- Дамп данных таблицы `tbl_user`
--

INSERT INTO `tbl_user` (`name`, `surname`, `fathername`, `id_user`, `shift`, `cabinet`, `id_special`, `block`, `password`) VALUES
('Администратор', '-', '-', 1, 1, 5, 5, 1, '11'),
('Иван', 'Иванов', 'Проверяевич', 2, 2, 2, 3, 1, '11'),
('Пётр', 'Петров', 'Обработович', 3, 1, 1, 2, 1, '11'),
('Сидр', 'Сидоров', 'Сканирович', 4, 1, 2, 1, 1, '11');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `tbl_freedoc`
--
ALTER TABLE `tbl_freedoc`
  ADD PRIMARY KEY (`id_doc`);

--
-- Индексы таблицы `tbl_list`
--
ALTER TABLE `tbl_list`
  ADD PRIMARY KEY (`id_doc`,`stage`);

--
-- Индексы таблицы `tbl_name`
--
ALTER TABLE `tbl_name`
  ADD PRIMARY KEY (`id_doc`),
  ADD UNIQUE KEY `Rubrika` (`rubrika`,`fond`,`opis`,`delo`);

--
-- Индексы таблицы `tbl_registrate`
--
ALTER TABLE `tbl_registrate`
  ADD UNIQUE KEY `id_user` (`id_user`,`date_in`),
  ADD UNIQUE KEY `id_user_2` (`id_user`,`date_out`);

--
-- Индексы таблицы `tbl_reverse`
--
ALTER TABLE `tbl_reverse`
  ADD UNIQUE KEY `Date_return` (`date_return`);

--
-- Индексы таблицы `tbl_specialize`
--
ALTER TABLE `tbl_specialize`
  ADD PRIMARY KEY (`id_special`);

--
-- Индексы таблицы `tbl_stage`
--
ALTER TABLE `tbl_stage`
  ADD PRIMARY KEY (`stage`);

--
-- Индексы таблицы `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `tbl_name`
--
ALTER TABLE `tbl_name`
  MODIFY `id_doc` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
