<!DOCTYPE html>
<html>
    <?php
    session_start();
    if (isset($_SESSION['a'])) {
        if ($_SESSION['a'] == "adm") {
            $id_ActiveUser = $_SESSION['id_ActiveUser'];
        }
    } else {
        die('�������� � �������!');
    };
    include './inc/#mysql.inc';
    include("inc/unit.inc");

    $Props = array();
    (isset($_GET['package']) ? $Props['pack'] = $_GET['package'] : $Props['pack'] = 'off');
    (isset($_GET['cabinet_in']) ? $Props['CabIn'] = $_GET['cabinet_in'] : $Props['CabIn'] = '2');
    (isset($_GET['shift']) ? $Props['shift'] = $_GET['shift'] : $Props['shift'] = '1');
    (isset($_GET['cabinet_out']) ? $Props['CabOut'] = $_GET['cabinet_out'] : $Props['CabOut'] = '2');
    (isset($_GET['pack_new']) ? $Props['pack_new'] = $_GET['pack_new'] : $Props['pack_new'] = 'off');
    (isset($_GET['rb_special']) ? $Props['special'] = $_GET['rb_special'] : $Props['special'] = '1');
    ?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
        <script src="JS/multi_onload.js" type="text/javascript"></script> 
        <link rel="stylesheet" type="text/css" href="style/Style_admin.css">
        <title>JArch</title>        
    </head>
    <body onload="setFocus">
        <script src="JS/multi_onload.js" type="text/javascript"></script>
        <script language="Javascript" type="text/javascript">
        function setFocus()
        {
            document.getElementById("F3").focus();
        }
        safeAddLoadFunction(setFocus);
        </script>
        <div id="admin_menu"><?php include './inc/menu.inc'; ?></div>
        <div id="Content">
            <div id="status">
                <text>����������������� - ���������� ���������� � ���� </text>
            </div>
            <Form name="fm_add" action="admEntry.php" method="GET">
                <div id="Process">
                    <div id="ADD_Panel">
                        <table border="1">
                            <tr>
                                <th>�������</th>
                                <th>����</th>
                                <th>�����</th>
                                <th>����</th>
                            </tr>
                            <tr>
                                <td>
                                    <input type = "text" name="F0" value="-">
                                </td>
                                <td>
                                    <input type = "text" name="F1" value="<?php echo (isset($_GET['F1']) ? $_GET['F1'] : ''); ?>">
                                </td>
                                <td>
                                    <input type = "text" name="F2" value="<?php echo (isset($_GET['F2']) ? $_GET['F2'] : ''); ?>">
                                </td>
                                <td>
                                    <input type = "text" name="F3" id="F3">
                                </td>
                            </tr>
                            <?php
                            if (isset($_GET['action']) && !empty($_GET['action'])) {
                                $action = $_GET['action'];
                                if ($action == "��������") {
                                    if (!empty($_GET['F0']) && !empty($_GET['F1']) && !empty($_GET['F2']) && !empty($_GET['F3'])) {
                                        $Doc = array(
                                            'rubrika' => $_GET['F0'],
                                            'fond' => $_GET['F1'],
                                            'opis' => $_GET['F2'],
                                            'delo' => $_GET['F3'],
                                            'CabIn' => $Props['CabIn'],
                                            'pack' => $Props['pack'],
                                            'pack_new' => $Props['pack_new']
                                        );
                                        AdminView::entryDeal($Doc);
                                    } else {
                                        echo "<script type=\"text/javascript\">alert('�������� ��������� ������� �� ���������!')</script>";
                                    }
                                }
                                if ($action == "������������") {
                                    AdminView::parametricDistrib($Props['special'], $Props['shift'], $Props['CabOut']);
                                }
                            }
                            ?>  
                        </table>
                        <div id="TextPro"></div>
                        <table border="1">
                            <tr>
                                <th> ������� </th>
                                <th> ����� </th>
                            </tr>
                            <tr>
                                <td>
                                    <label for="rbutt1">�1</label>
                                    <input type="radio" name="cabinet_in" id="rbutt1" value="1" <?php echo ($Props['CabIn'] == '1' ? "checked" : ""); ?> >                                    
                                </td>
                                <td>
                                    <input type="checkbox" name="package" value="on" <?php echo ($Props['pack'] == 'on' ? "checked" : ""); ?> >
                                    Now
                                </td>
                            <tr/>
                            <tr>
                                <td>
                                    <label for="rbutt2">�2</label>
                                    <input type="radio" name="cabinet_in" id="rbutt2" value="2" <?php echo ($Props['CabIn'] == '2' ? "checked" : ""); ?> >
                                </td>                                    
                                <td>
                                    <input type="checkbox" name="pack_new" value="on">
                                    New
                                </td>
                            </tr>                        
                        </table>
                        <input id="Bt_finish" type="submit" name="action" value="��������" />
                    </div>
                    <div id="Destrib">
                        <table border="1">
                            <tr>
                                <th>�����</th>
                                <th>�������</th>
                            </tr>
                            <tr>
                                <td>
                                    <label for="rbutt3"> 1 </label>
                                    <input type="radio" name="shift" id="rbutt3" value="1" <?php echo ($Props['shift'] == '1' ? "checked" : ""); ?> >                                    
                                </td>
                                <td>
                                    <label for="rbutt5">�1</label>
                                    <input type="radio" name="cabinet_out" id="rbutt5" value="1" <?php echo ($Props['CabOut'] == '1' ? "checked" : ""); ?> >
                                </td>

                            </tr>
                            <tr>
                                <td>    
                                    <label for="rbutt4"> 2 </label>
                                    <input type="radio" name="shift" id="rbutt4" value="2" <?php echo ($Props['shift'] == '2' ? "checked" : ""); ?> >
                                </td>
                                <td>
                                    <label for="rbutt6">�2</label>
                                    <input type="radio" name="cabinet_out" id="rbutt6" value="2" <?php echo ($Props['CabOut'] == '2' ? "checked" : ""); ?> >
                                </td>
                            </tr>                        
                        </table>
                        <table border="1">
                            <tr>
                                <th><input type="radio" name="rb_special" value="1" checked /></th>
                                <th>������������</th>
                            </tr>
                            <tr>
                                <th><input type="radio" name="rb_special" value="2" /></th>
                                <th>���������</th>
                            </tr>
                            <tr>
                                <th><input type="radio" name="rb_special" value="3" /></th>
                                <th>��������</th>
                            </tr> 
                        </table>
                        <input id="Bt_distrib" type="submit" name="action" value="������������"/>                        
                    </div>                   
                </div>             
            </form>
            <Form action="index.php" method="post">
                <div id="Pannel">                    
                    <input type="submit" name="action" value="�����" title=""/>
                </div>
            </form>            
        </div>
        <script type="text/javascript">
            safeAddLoadFunction(setFocus);
        </script>
    </body>
</html>
