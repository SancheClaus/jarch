<!DOCTYPE html>
<html>
    <?php
    session_start();
    $path = array(
        'scan' => 'usrFinishScan.php',
        'proc' => 'usrProc.php',
        'check' => 'usrCheck.php'
    );
    if (isset($_SESSION['a']) && array_key_exists($_SESSION['a'], $path)) {
        $id_ActiveUser = $_SESSION['id_ActiveUser'];
        $id_special = $_SESSION['id_special'];
    } else {
        die('�������� � �������!');
    };
    include './inc/#mysql.inc';
    include("inc/unit.inc");
    ?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
        <link rel="stylesheet" type="text/css" href="style/Style_user.css">
        <title>JArch</title>
    </head>
    <body>
        <div id="Content">
            <div id="status">
                <text> ����� </text>
            </div>
            <Form action="usrFinishProc.php" method="post">
                <div id="Process">                
                    <div id="TextPro">
                        <table border="1">
                            <tr>
                                <th>���������</th>
                                <th>���</th>
                                <th>�������</th>
                                <th>�����</th>
                                <th>�����</th>
                            </tr>
                            <?php
                            $SumL = 0;
                            $SumD = 0;
                            if ($_SESSION['a'] != "scan") {
                                if (isset($_POST['CountField'])) {
                                    $CountField = $_POST['CountField'];
                                    for ($i = 1; $i <= $CountField; $i++) {
                                        if (isset($_POST['fn_' . $i])) { // ��� �������
                                            if ((isset($_POST['Prl_' . $i])) && (isset($_POST['id_doc' . $i]))) {
                                                // ���� ���������� - ��������� ������� ������
                                                (isset($_POST['Sz_Mc' . $i]) ? $Size_Mc = $_POST['Sz_Mc' . $i] : $Size_Mc = 0);
                                                (isset($_POST['Sz_Wc' . $i]) ? $Size_Wc = $_POST['Sz_Wc' . $i] : $Size_Wc = 0);
                                                if ($id_special == 2) {
                                                    if (!($Size_Mc && $Size_Wc || UserPrint::isReverse($id_doc_ending))) {
                                                        echo "<input type=\"hidden\" name=\"listR\" value=\"\"/>"
                                                        . "<script type=\"text/javascript\">alert('"
                                                        . "�� ������ ������ ������-����� ��� ������� ����� ���������."
                                                        . " �� �������, �� ���������� �����.');</script>";
                                                        header("Refresh: 0;  url=usrProc.php");
                                                        exit();
                                                    }
                                                }

                                                $id_doc = $_POST['id_doc' . $i];
                                                $lists = $_POST['Prl_' . $i];
                                                $SumL += $lists;
                                                $SumD++;
                                                UserPrint::finishDealProc($id_ActiveUser, $id_doc, $id_special, $lists, $Size_Mc, $Size_Wc);
                                            } elseif (UserPrint::isReverse($id_doc)) {
                                                UserPrint::finishDealProc($id_ActiveUser, $id_doc, $id_special);
                                            }
                                        }
                                    }
                                    UserPrint::DDP($id_ActiveUser, $id_special);
                                }
                            }
                            $Report = Report::getReportUser($id_ActiveUser, $id_special);
                            ?>                            
                            <tr>
                                <td>�������</td>
                                <td><?= $Report['d']['docs'] ?></td>
                                <td><?= $Report['d']['lists'] ?></td>                                
                                <td><?= Report::getNorm($id_special) ?></td>
                                <td> <a href="usrReport.php?date=d"> ����� </a> </td>
                            </tr>
                            <tr>
                                <td>�� �����</td>
                                <td><?= $Report['m']['docs'] ?></td>
                                <td><?= $Report['m']['lists'] ?></td>                                
                                <td> - </td>
                                <td> <a href="usrReport.php?date=m"> ����� </a></td>
                            </tr>
                        </table>                  
                    </div>
                    <input name="ending_deal" type="hidden" id="hidden" value="">
                    <a href="<?= $path[$_SESSION['a']] ?>"> << ��������� �� ���������� </a>
                </div>
                <div id="FotoScreen">
                </div>
            </form>
            <div id="Pannel">
                <Form action="index.php" method="post">                    
                    <input type="submit" name="action" value="�����"/>                    
                </form>
            </div>
        </div>
    </body>
</html>
