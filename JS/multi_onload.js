
    onLoadFuncton=new Array();
     
    function safeAddLoadFunction(myFunction) {
        
      if (typeof(myFunction) != 'function') {
        return false;
      }
      
      if (window.onload) {
        
        if (onLoadFuncton.length) {
          // Add new function
          onLoadFuncton[onLoadFuncton.length]=new function() {
            myFunction();
          }
        }
        else {
          // Add first function
          oldOnLoadFunction=window.onload;
          onLoadFuncton[onLoadFuncton.length]=new function() {
            oldOnLoadFunction();
          }
          // Add new function
          onLoadFuncton[onLoadFuncton.length]=new function() {
            myFunction();
          }
        } 
      }
      else {
        // Add new function
        onLoadFuncton[onLoadFuncton.length]=new function() {
          myFunction();
        }
      }
      // Set global onLoad function
      window.onload=function() { safeDoLoadEvents(); }
      return true;
    }
     
    // Execute all onLoad functions
    function safeDoLoadEvents() {
      for (i=0; i<onLoadFuncton.length; i++) {
        onLoadFuncton[i];
      }
    }

