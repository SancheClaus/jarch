<?php

class UserPrint {

    public static function getUsers() {
        $query = "SELECT `name`, `surname`, `id_user` FROM `tbl_user` "
                . "WHERE NOT `block` = '0' ORDER BY `surname`";
        $sql = mysql_query($query);
        $Employees = array();

        while ($dp = mysql_fetch_object($sql)) {
            $Employees[] = array(
                'name' => $dp->name,
                'surname' => $dp->surname,
                'id' => $dp->id_user
            );
        }
        return $Employees;
    }

    public static function Autorize($id_user, $pswd) {
        $q = "SELECT COUNT(*) FROM `tbl_user` "
                . "WHERE `id_user` = '$id_user' and `password` = '$pswd' ";
        $sql = mysql_query($q);
        $res = mysql_fetch_row($sql);
        if ($res[0] == 1) {
            $now = new DateTime();
            $date = $now->format('Y-m-d H:i:s');
            mysql_query("INSERT INTO tbl_registrate (id_user, date_in) "
                    . "VALUES ('$id_user', '$date')");
            $q = "SELECT u.id_special, s.short_name FROM tbl_user u, tbl_specialize s "
                    . "WHERE u.id_user = '$id_user' AND u.id_special = s.id_special";
            $sql = mysql_query($q);
            $dp = mysql_fetch_object($sql);

            session_start();
            $_SESSION['id_ActiveUser'] = $id_user;
            $_SESSION['id_special'] = $dp->id_special;
            $_SESSION['a'] = $dp->short_name;
            $links = array(
                'scan' => "usrFinishScan.php",
                'proc' => "usrProc.php",
                'check' => "usrCheck.php",
                'adm' => "admEntry.php"
            );
            if (array_key_exists($_SESSION['a'], $links)) {
                header("Refresh: 0;  url=" . $links[$_SESSION['a']]);
                echo "��������...";
            } else {
                die('�������� � �������! ���������� � ��������������.');
            }
        }
    }

    public static function closeSesion($id_user) {
        $now = new DateTime();
        $date = $now->format('Y-m-d H:i:s');

        $update = "UPDATE `tbl_registrate` SET `date_out` = '$date' "
                . "WHERE `id_user` = '$id_user' ";
        $dummy = mysql_query($update);
        $_SESSION['a'] = "false";
    }

    public static function getDeal($id_doc) {
        $q = "SELECT `rubrika`, `fond`, `opis`, `delo` "
                . "FROM `tbl_name` WHERE `id_doc` = '$id_doc' ";
        $sql = mysql_query($q);
        $dp = mysql_fetch_object($sql);
        $Deal = array(
            'rubrika' => $dp->rubrika,
            'fond' => $dp->fond,
            'opis' => $dp->opis,
            'delo' => $dp->delo
        );
        return $Deal;
    }

    public static function printOneDeal(array $Deal) {
        echo '<td>' . $Deal['rubrika'] . '</td>
              <td>' . $Deal['fond'] . '</td>
              <td>' . $Deal['opis'] . '</td>
              <td>' . $Deal['delo'] . '</td>';
    }

    public static function isReverse($id_doc) {
        $q = "SELECT * FROM `tbl_reverse` WHERE `id_doc` = '$id_doc' AND `complete` = '0'";
        $sql = mysql_query($q);
        return mysql_fetch_object($sql);
    }

    public static function getMsgReverse($id_doc) {
        $q = "SELECT `text` FROM `tbl_reverse` WHERE `id_doc` = '$id_doc'"
                . " AND `Complete` = '0' ORDER BY `date_return` DESC";
        $dp = mysql_fetch_object(mysql_query($q));
        (isset($dp->text) ? $text = $dp->text : $text = "");
        return $text;
    }

    public static function getListMes($id_user, $id_special) {
        $data = array();
        $q = "SELECT f.id_user, f.id_doc, r.Sender, r.text FROM tbl_freedoc f, tbl_reverse r "
                . "WHERE f.id_user = '$id_user' AND f.stage = '$id_special' "
                . "AND r.id_doc = f.id_doc AND (r.catcher = '$id_user' OR r.sender = '$id_user')";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql)) {
            $id_doc = $dp->id_doc;
            ($dp->Sender == $id_user ? $tag = "����������" : $tag = "�������");
            $text = $dp->text;
            if (empty($text)) {
                $text = "�����������";
            }

            $data[] = array(
                'id_doc' => $id_doc,
                'tag' => $tag,
                'text' => $text,
                'deal' => UserPrint::getDeal($id_doc)
            );
        }
        return $data;
    }

    public static function printListMes($id_user, $id_special) {
        $data = UserPrint::getListMes($id_user, $id_special);
        foreach ($data as $item) {
            echo $item['tag'] . ": " . $item['deal']['fond'] . " - "
            . $item['deal']['opis'] . " - " . $item['deal']['delo']
            . "\n���������: " . $item['text'] . "\n";
        }
    }

    private static function getTaskList($id_user, $stage, $active_deal = 0) {
        $deals = array();
        $q = "SELECT f.id_doc, n.rubrika, n.fond, n.opis, n.delo "
                . "FROM tbl_freedoc f, tbl_name n "
                . "WHERE f.id_user = '$id_user' AND f.stage = '$stage' "
                . "AND f.id_doc = n.id_doc ";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql)) {
            $id_doc = $dp->id_doc;
            if ($id_doc == $active_deal) {
                $status = "�����������";
            } elseif (UserPrint::isReverse($id_doc)) {
                $status = "�������";
            } else {
                $status = "��������";
            }
            $deals[] = array(
                'id_doc' => $id_doc,
                'rubrika' => $dp->rubrika,
                'fond' => $dp->fond,
                'opis' => $dp->opis,
                'delo' => $dp->delo,
                'status' => $status
            );
        }
        return $deals;
    }
    
    public static function printTaskList($id_user, $stage, $link = 'index.php', $enable = 1, $active_deal = 0) {
        $Deals = UserPrint::getTaskList($id_user, $stage, $active_deal);
        foreach ($Deals as $Deal) {
            echo '<tr>';
            UserPrint::printOneDeal($Deal);
            if ($enable) {
                echo '<td><a href="' . $link . '?id_doc=' . $Deal['id_doc'] . '">' . $Deal['status'] . '</a></td></tr>';
            } else {
                echo '<td>' . $Deal['status'] . '</td></tr>';
            }
        }
    }

    private static function getTaskListPC($id_user, $stage) {
        $Docs = UserPrint::getTaskList($id_user, $stage);
        foreach ($Docs as $id_doc => &$Doc) {
            $Doc['scan'] = UserPrint::whoDidIt($Doc['id_doc']);
            if ($stage == 3) {
                $Doc['proc'] = UserPrint::whoDidIt($Doc['id_doc'], 2);
            }
        }
        return $Docs;
    }

    public static function printTaskListPC($id_user, $stage = 2) {
        $Docs = UserPrint::getTaskListPC($id_user, $stage);
        $i = 0;
        foreach ($Docs as $id_doc => $Doc) {
            $i++;
            echo '<tr>';
            UserPrint::printOneDeal($Doc);
            echo '<td>' . $Doc['scan']['surname'] . '</td>';
            if ($stage == 3) {
                echo '<td>' . $Doc['proc']['surname'] . '</td>';
            }
            echo '<td><input type=\'text\' size=6 name=\'Prl_' . $i . '\' value=\'\'/></td>';
            if ($stage == 2) { // ���� ��� ������������ ������� ����� (��) �������� ������ ������������
                echo '<td><input type=\'number\' step=\'0.01\' name=\'Sz_Mc' . $i . '\' value=\'\'/></td>
                    <td><input type=\'number\' step=\'0.01\' name=\'Sz_Wc' . $i . '\' value=\'\'/></td>';
            }
            echo '<td><input type=\'checkbox\' name=\'fn_' . $i . '\' value=\'on\'></td>
                        <td><a href=\'usrReverse.php?d=' . $Doc['id_doc'] . '\'>�������</a></td>
                        <input type=\'hidden\' name=\'id_doc' . $i . '\' value=\'' . $Doc['id_doc'] . '\'>';
        }
        echo '<input type=\'hidden\' name=\'CountField\' value=\'' . $i . '\'>';
    }

    public static function whoDidIt($id_doc, $id_special = 1) {
        $user = array();
        $q = "SELECT l.id_user, u.surname  FROM tbl_list l, tbl_user u "
                . "WHERE l.id_doc = '$id_doc' AND l.stage = '$id_special' "
                . "AND u.id_user = l.id_user ";
        $sql = mysql_query($q);
        if ($dp = mysql_fetch_object($sql)) {
            $user['id'] = $dp->id_user;
            $user['surname'] = $dp->surname;
        }
        return $user;
    }

    public static function finishDealProc($id_user, $id_doc_ending, $stage, $list = 0, $size_mc = 0, $size_wc = 0) {
        $stage_next = $stage + 1;
        $q = "SELECT `sender`  FROM `tbl_reverse` WHERE `catcher` = '$id_user' "
                . "AND `complete` = '0' AND `id_doc` = '$id_doc_ending' ";
        $sql = mysql_query($q);
        if ($dp = mysql_fetch_object($sql)) {
            $sender = $dp->sender;
            mysql_query("UPDATE tbl_reverse SET complete = '1' WHERE id_doc = '$id_doc_ending' "
                    . "AND `catcher` = '$id_user' ");
            mysql_query("UPDATE tbl_freedoc SET stage = '$stage_next', id_user = '$sender' "
                    . "WHERE id_doc = '$id_doc_ending'");
            return;
        }
        if ($size_mc && $size_wc) { // ���� �� ������ �������
            mysql_query("UPDATE tbl_freedoc SET size_mc = '$size_mc', size_wc = '$size_wc' "
                    . "WHERE id_doc = '$id_doc_ending'");
        }
        mysql_query("UPDATE tbl_freedoc SET stage = '$stage_next', id_user = '0' WHERE id_doc = '$id_doc_ending'");
        mysql_query("INSERT INTO tbl_list (id_doc, id_user, count, stage) "
                . "VALUES ('$id_doc_ending', '$id_user', '$list', '$stage')");
    }

    public static function DDS($id_user, $stage, $limit = 5) {
        $count = AdminView::countDeals($id_user);
        $q = "SELECT f.id_doc, f.package FROM tbl_user u, tbl_freedoc f "
                . "WHERE u.id_user = '$id_user' AND f.id_user = '0' "
                . "AND f.stage = '$stage' AND f.cabinet = u.cabinet ";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql) and ( $count < $limit)) {
            $id_doc = $dp->id_doc;
            $id_package = $dp->package;

            if ($id_package) {
                mysql_query("UPDATE `tbl_freedoc` SET `id_user` = '$id_user' WHERE `package` = '$id_package' "
                        . "AND `stage` = '$stage' AND `id_user` = '0'");
                $count = AdminView::countDeals($id_user);
            } else {
                mysql_query("UPDATE `tbl_freedoc` SET `id_user` = '$id_user' WHERE `id_doc` = '$id_doc' "
                        . "AND `stage` = '$stage' AND `id_user` = '0' ");
                $count++;
            }
        }
    }

    public static function DDP($id_user, $stage, $limit = 10) {
        $count = AdminView::countDeals($id_user);
        $q = "SELECT `id_doc`, `package` FROM `tbl_freedoc` "
                . "WHERE `id_user` = '0' AND `stage` = '$stage' ";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql) and ( $count < $limit)) {
            $id_doc = $dp->id_doc;
            $id_package = $dp->package;

            if ($id_package) {
                mysql_query("UPDATE `tbl_freedoc` SET `id_user` = '$id_user' "
                        . "WHERE `package` = '$id_package' AND `stage` = '$stage' AND `id_user` = '0' ");
                $count = AdminView::countDeals($id_user);
            } else {
                mysql_query("UPDATE `tbl_freedoc` SET `id_user` = '$id_user' "
                        . "WHERE `id_doc` = '$id_doc' AND `stage` = '$stage'  AND `id_user` = '0' ");
                $count++;
            }
        }
    }

    public static function ReverseDeal($id_sender, $id_doc, $id_special, $mes = "") {
        $stage = $id_special - 1;
        $user_catcher = UserPrint::whoDidIt($id_doc, $stage);
        mysql_query("INSERT INTO `tbl_reverse` (`sender`, `catcher`, `id_doc`, `text`) "
                . "VALUES ('$id_sender', '$user_catcher[id]', '$id_doc', '$mes')");
        mysql_query("UPDATE `tbl_freedoc` SET `id_user` = '$user_catcher[id]', `stage` = '$stage' "
                . "WHERE `id_doc` = '$id_doc'");
        UserPrint::DDP($id_sender, $id_special);
    }

}

class AdminView {

    public static function setBlock($id_user) {
        $q = "SELECT block FROM tbl_user WHERE id_user = '$id_user' ";
        $sql = mysql_query($q);
        $dp = mysql_fetch_object($sql);
        $block = $dp->block;
        ($block == 0 ? $block++ : $block--);

        $q = "UPDATE tbl_user SET block = '$block' WHERE id_user = '$id_user' ";
        $sql = mysql_query($q);
    }

    public static function getUsers() {
        $q = "SELECT u.*, s.specialize FROM tbl_user u, tbl_specialize s "
                . "WHERE u.id_special = s.id_special ORDER BY id_special DESC";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql)) {
            $Employees[] = array(
                'id_user' => $dp->id_user,
                'name' => $dp->name,
                'surname' => $dp->surname,
                'fathername' => $dp->fathername,
                'id_special' => $dp->specialize,
                'shift' => $dp->shift,
                'cabinet' => $dp->cabinet,
                'block' => $dp->block
            );
            $last = count($Employees) - 1;

            echo '</td><td>' . $Employees[$last]['surname']
            . '</td><td>' . $Employees[$last]['name']
            . '</td><td>' . $Employees[$last]['fathername']
            . '</td><td>' . $Employees[$last]['id_special']
            . '</td><td>' . $Employees[$last]['shift']
            . '</td><td>' . $Employees[$last]['cabinet'] . '</td></td>';

            if ($Employees[$last]['block'] >= 1) {
                echo '<td><a href=\'admListUsers.php?Block='
                . $Employees[$last]['id_user']
                . '\'>�����������</a></td>';
            } elseif ($Employees[$last]['block'] == 0)
                echo '<td><a href=\'admListUsers.php?Block='
                . $Employees[$last]['id_user']
                . '\'>��������������</a></td>';
            echo '<td><a href=\'admListUsers.php?Edit='
            . $Employees[$last]['id_user']
            . '\'>��������</a></td></tr>';
        }
    }

    public static function getUser($id_user) {
        $q = "SELECT * FROM tbl_user WHERE id_user = '$id_user' ";
        $sql = mysql_query($q);
        $dp = mysql_fetch_object($sql);
        $User = array(
            'id' => $id_user,
            'surname' => $dp->surname,
            'name' => $dp->name,
            'fathername' => $dp->fathername,
            'id_special' => $dp->id_special,
            'shift' => $dp->shift,
            'cabinet' => $dp->cabinet,
            'pass' => $dp->password,
        );

        return $User;
    }

    public static function addNewUser(array $User) {
        if (!empty($User['id_user'])) {
            $q = "UPDATE `tbl_user` SET `name` = '$User[name]', `surname` = '$User[surname]', "
                    . "`fathername` = '$User[fathername]', `id_special` = '$User[id_special]', "
                    . "`shift` = '$User[shift]', `cabinet` = '$User[cabinet]', `password` = '$User[pass]' "
                    . "WHERE id_user = '$User[id_user]' ";
            $sql = mysql_query($q);
            return;
        }
        $q = "INSERT INTO `tbl_user` (`name`, `surname`, `fathername`, "
                . "`id_special`, `shift`, `cabinet`, `password`) "
                . "VALUES ('$User[name]', '$User[surname]', '$User[fathername]', "
                . "'$User[id_special]', '$User[shift]', '$User[cabinet]', '$User[pass]')";
        $sql = mysql_query($q);
    }

    /*
      public static function getAddDoc($Rubrika, $Fond, $Opis, $Delo) {
      $Doc = array(
      'Rubrika' => $Rubrika,
      'Fond' => $Fond,
      'Opis' => $Opis,
      'Delo' => $Delo
      );
      return $Doc;
      }
     */

    public static function searchDeal($rubrika, $fond, $opis = "", $delo = "", $stage = 1) {
        $Data = array();
        $Parameters = array(
            'opis' => ($Opis ? " AND n.opis = '$opis' " : ""),
            'delo' => ($delo ? " AND n.delo = '$delo' " : "")
        );

        if ($Parameters['opis'] && $Parameters['delo']) {
            $q = "SELECT n.id_doc FROM tbl_name n "
                    . "WHERE n.rubrika = '$rubrika' AND n.fond = '$fond'"
                    . $Parameters['opis'] . $Parameters['delo'];
            $sql = mysql_query($q);
            while ($dp = mysql_fetch_object($sql)) {
                $data = array(
                    'id' => $dp->id_doc,
                );
            } if (empty($Data)) {
                echo " �� ������� ���������� �� �������: " . $rubrika . "-" . $fond . "-" . $opis . "-" . $delo;
                return;
            }

            $q = "SELECT l.*, u.* FROM tbl_list l, tbl_user u "
                    . "WHERE l.id_user = u.id_user AND l.id_doc = '$Data[id]'";
            $sql = mysql_query($q);
            while ($dp = mysql_fetch_object($sql)) {
                $data['proces'][] = array(
                    '����' => $dp->stage,
                    '��������' => $dp->surname,
                    '������' => $dp->count,
                    '���������' => $dp->date_proc,
                );
            }

            $q = "SELECT f.stage, f.id_user, s.stage_name FROM tbl_freedoc f, tbl_stage s "
                    . "WHERE f.id_doc = '$Data[id]' AND f.stage = s.stage";
            $sql = mysql_query($q);
            $dp = mysql_fetch_object($sql);
            $Data['status'] = $dp->stage_name;

            return $Data;
        } else {
            // ����� ���������� ��� ����� (��� �����) � ����� ����� ������
            $q = "SELECT COUNT(l.count) AS deals, SUM(l.count) AS lists FROM tbl_name n, tbl_list l "
                    . "WHERE n.rubrika = '$rubrika' AND n.fond = '$fond' " . $Parameters['opis']
                    . " AND l.stage = '$stage' AND n.id_doc = l.id_doc";
            ;

            $sql = mysql_query($q);
            $Data = array(
                'deals' => mysql_result($sql, 0, 'deals'),
                'lists' => mysql_result($sql, 0, 'lists')
            );

            if (empty($Data)) {
                echo " �� ������� ���������� �� �������: " . $rubrika . "-" . $fond . "-" . $opis . "-" . $delo;
                return;
            } else {
                echo "���: " . $Data['deals'] . "<br/> �������: " . $Data['lists'];
                return;
            }
        }
    }

    public static function entryDeal(array $Doc) {
        $q = "SELECT `rubrika`, `fond`, `opis`, `delo` FROM `tbl_name` "
                . "WHERE `rubrika` = '$Doc[rubrika]' and `fond` = '$Doc[fond]' "
                . "and `opis` = '$Doc[opis]' and `delo` = '$Doc[delo]'";
        $sql = mysql_query($q);
        if (mysql_fetch_object($sql)) {
            echo "<script type=\"text/javascript\">alert('���� ��� ���������� � ����!')</script>";
            return;
        }

        $q = "SELECT MAX(`id_doc`) FROM `tbl_name`";
        $sql = mysql_query($q);
        $dp = mysql_fetch_object($sql);
        $id_doc = mysql_result($sql, 0) + 1;

        mysql_query("INSERT INTO `tbl_name` (`id_doc`, `rubrika`, `fond`, `opis`, `delo`) "
                . "VALUES ('$id_doc','$Doc[rubrika]', '$Doc[fond]', '$Doc[opis]', '$Doc[delo]')");

        self::putDocInReserve($id_doc, $Doc['CabIn'], $Doc['pack'], $Doc['pack_new']);
        echo "<tr>";
        UserPrint::printOneDeal($Doc);
        echo "</tr>";
    }

    private static function putDocInReserve($id_doc, $cabinet, $package, $pack_new) {
        if ($package == 'on') {
            $q = "SELECT MAX(package) FROM tbl_freedoc";
            $sql = mysql_query($q);
            $id_package = mysql_result($sql, 0); // id ��������� �����

            $q = "SELECT `package` FROM `tbl_freedoc` ORDER BY  `id_doc` DESC LIMIT 1";
            $sql = mysql_query($q);
            $dp = mysql_fetch_object($sql);
            $last_deal_package = $dp->package;

            if (($id_package == 0) OR ( $id_package != $last_deal_package) OR ( $pack_new == 'on')) {
                $id_package++;
            }
            $query_in = "INSERT INTO `tbl_freedoc` (`id_doc`, `id_user`, `cabinet`, `package`) "
                    . "VALUES ('$id_doc', '0', '$cabinet', '$id_package')";
            $sql_in = mysql_query($query_in);
        } else {
            $q = "INSERT INTO `tbl_freedoc` (`id_doc`, `id_user`, `cabinet`) "
                    . "VALUES ('$id_doc', '0', '$cabinet')";
            $sql = mysql_query($q);
        }
    }

    public static function countDeals($id_user) {
        $q = "SELECT `id_special`  FROM `tbl_user` WHERE `id_user` = '$id_user'";
        $sql = mysql_query($q);
        $dp = mysql_fetch_object($sql);
        $id_special = $dp->id_special;
        $q = "SELECT COUNT(*) FROM `tbl_freedoc` "
                . "WHERE `id_user` = '$id_user' AND `stage` = '$id_special' ";
        $sql = mysql_query($q);
        return mysql_result($sql, 0);
    }

    public static function defaultDistrib($id_user, $limit = 4) {
        $q = "SELECT `id_special`, `cabinet` FROM tbl_user WHERE id_user = '$id_user'";
        $sql = mysql_query($q);
        $dp = mysql_fetch_object($sql);
        $stage = $dp->id_special;
        $cabinet = $dp->cabinet;
        $count = self::countDeals($id_user);

        if ($stage == 1) {
            $q = "SELECT `id_doc`, `package` FROM `tbl_freedoc` "
                    . "WHERE `id_user` = '0' AND `stage` = '$stage' AND `cabinet` = '$cabinet' ";
        } else {
            $q = "SELECT `id_doc`, `package` FROM `tbl_freedoc` "
                    . "WHERE `id_user` = '0' and `stage` = '$stage' ";
        }
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql) and ( $count < $limit)) {
            $id_doc = $dp->id_doc;
            $id_package = $dp->package;
            if (!$id_package == 0) {
                $q_up = "UPDATE `tbl_freedoc` SET `id_user` = '$id_user' "
                        . "WHERE `package` = '$id_package' AND `stage` = '$stage' AND `id_user` = '0' ";
                $sql_up = mysql_query($q_up);
            } else {
                $q_up = "UPDATE `tbl_freedoc` SET `id_user` = '$id_user' "
                        . "WHERE id_doc = '$id_doc' AND stage = '$stage' ";
                $sql_up = mysql_query($q_up);
            }
            $count++;
        }
    }

    public static function parametricDistrib($stage, $shift, $cabinet, $limit = 2) {
        $q = "SELECT * FROM `tbl_user` WHERE `id_special` = '$stage' "
                . "AND `shift` = '$shift' AND `cabinet` = '$cabinet' ";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql)) {
            self::defaultDistrib($dp->id_user);
        }
    }

    private static function getUsersDeal($id_special) {
        $q = "SELECT * FROM tbl_user WHERE id_special = $id_special ORDER BY shift";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql)) {
            $Users[] = array(
                'surname' => $dp->surname,
                'id' => $dp->id_user,
                'shift' => $dp->shift,
                'cabinet' => $dp->cabinet,
                'count' => self::countDeals($dp->id_user),
            );
        }
        return $Users;
    }

    public static function printUsersDeal($id_special) {
        $Users = self::getUsersDeal($id_special);
        foreach ($Users as $User) {
            if ($User['count'] > 1) {
                echo '<tr bgcolor=\'#ccff66\'><td>' . $User['surname'] . '</td><td>' . $User['cabinet']
                . '</td><td>' . $User['shift'] . '</td><td>' . $User['count']
                . '</td><td><a href=\'admListDeal.php?AD=' . $User['id']
                . '\'>��������</a></td>';
            } elseif ($User['count'] > 0) {
                echo '<tr bgcolor=\'#ffff66\'><td>' . $User['surname'] . '</td><td>' . $User['cabinet']
                . '</td><td>' . $User['shift'] . '</td><td>' . $User['count']
                . '</td><td><a href=\'admListDeal.php?AD=' . $User['id']
                . '\'>��������</a></td>';
            } else {
                echo '<tr bgcolor=\'#ff6666\'><td>' . $User['surname'] . '</td><td>' . $User['cabinet'] . '</td><td>'
                . $User['shift'] . '</td><td>' . $User['count']
                . '</td><td><a href=\'admListDeal.php?AD=' . $User['id']
                . '\'>��������</a></td>';
            }
        }
    }

    private static function expReserveScan() {
        $q = "SELECT COUNT(*) FROM tbl_freedoc WHERE id_user = '0' and stage = '1' and cabinet = '1' ";
        $sql = mysql_query($q);
        $Data = array('Cab1' => mysql_result($sql, 0));

        $q = "SELECT COUNT(*) FROM tbl_freedoc WHERE id_user = '0' and stage = '1' and cabinet = '2' ";
        $sql = mysql_query($q);
        $Data['Cab2'] = mysql_result($sql, 0);

        return $Data;
    }

    private static function expReserveOther($stage) {
        $q = "SELECT COUNT(*) FROM tbl_freedoc WHERE id_user = '0' and stage = $stage";
        $sql = mysql_query($q);
        return mysql_result($sql, 0);
    }

    private static function getReserveInfo() {
        $Data = self::expReserveScan();
        $Reserve = array(
            '������������ (1 ���.)' => $Data['Cab1'],
            '������������ (2 ���.)' => $Data['Cab2'],
            '���������' => self::expReserveOther(2),
            '��������' => self::expReserveOther(3),
        );
        return $Reserve;
    }

    public static function printReserveInfo() {
        $Reserve = self::getReserveInfo();
        if (isset($Reserve)) {
            foreach ($Reserve as $key => $value) {
                echo "$key: $value <br/>";
            }
        }
    }

}

class Report {

    private static function getReport($id_user, $id_special, $period) {
        $Report = array();
        $Date = array(
            'd' => date("Y-m-d"),
            'm' => date("Y-m") . "-01",
            'a' => "0"
        );
        $q = "SELECT l.count, n.rubrika, n.fond, n.opis, n.delo "
                . "FROM tbl_list l, tbl_name n WHERE l.id_user = '$id_user' "
                . "AND l.stage = '$id_special' AND l.date_proc >= '$Date[$period]' "
                . "AND l.id_doc = n.id_doc ORDER BY l.date_proc DESC";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql)) {
            $Report[] = array(
                'rubrika' => $dp->rubrika,
                'fond' => $dp->fond,
                'opis' => $dp->opis,
                'delo' => $dp->delo,
                'count' => $dp->count
            );
        }
        return $Report;
    }

    public static function printReport($id_user, $id_special, $period = "d") {
        $data = Report::getReport($id_user, $id_special, $period);
        foreach ($data as $item) {
            echo "<tr>";
            UserPrint::printOneDeal($item);
            echo "<td>" . $item['count'] . "</td></tr>";
        }
    }

    public static function getNorm($id_special) {
        $Norma = array(
            2 => 3200,
            3 => 6400
        );
        return (array_key_exists($id_special, $Norma) ? $Norma[$id_special] : 0);
    }

    public static function getReportUser($id_user, $id_special) {
        $date = array(
            'd' => date("Y-m-d"),
            'm' => date("Y-m") . "-01",
        );
        $Report = array();

        foreach ($date as $key => $item) {
            $q = "SELECT SUM(`count`) FROM `tbl_list` WHERE `id_user` = '$id_user' "
                    . "AND `stage` = '$id_special' AND `date_proc` >= '$item'";
            $sql = mysql_query($q);
            $Report[$key]['lists'] = mysql_result($sql, 0);

            $q = "SELECT COUNT(id_doc) FROM `tbl_list` WHERE `id_user` = '$id_user' "
                    . "AND `stage` = '$id_special' AND `date_proc` >= '$item'";
            $sql = mysql_query($q);
            $Report[$key]['docs'] = mysql_result($sql, 0);
        }
        return $Report;
    }

    private static function getReportOnColor($id_special, $period, $format) {
        $Date = array(
            'day' => date("Y-m-d"),
            'month' => date("Y-m") . "-01",
        );
        $Color = array(
            1 => 'gray',
            2 => 'color'
        );
        $Users = array();

        $q = "SELECT id_user, surname  FROM tbl_user WHERE id_special = '$id_special'";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql)) {
            $Users[] = array(
                'id_user' => $dp->id_user,
                'surname' => $dp->surname
            );
        }
        if ($format == "individual") {
            foreach ($Users as &$User) {
                foreach ($Color as $key => $color) {
                    $q = "SELECT SUM(l.count) FROM tbl_list l, tbl_freedoc f WHERE l.id_user = '$User[id_user]' "
                            . "AND l.stage = '$id_special' AND l.date_proc >= '$Date[$period]]' "
                            . "AND f.id_doc = l.id_doc AND f.cabinet = '$key' ";
                    $sql = mysql_query($q);
                    $User[$color] = (empty(mysql_result($sql, 0)) ? 0 : mysql_result($sql, 0));
                }
            }
        } else { //  ��������� ������ id ���������� � ������� �� �������, �� ���������
            foreach ($Users as &$User) {
                foreach ($Color as $key => $color) {
                    //$User[$color] = array();
                    $q = "SELECT l.id_doc, l.count FROM tbl_list l, tbl_freedoc f WHERE l.id_user = '$User[id_user]' "
                            . "AND l.stage = '$id_special' AND l.date_proc >= '$Date[$period]]' "
                            . "AND f.id_doc = l.id_doc AND f.cabinet = '$key' ";
                    $sql = mysql_query($q);
                    while ($dp = mysql_fetch_object($sql)) {
                        $User[$color][] = array(
                            'id_doc' => $dp->id_doc,
                            'count' => $dp->count
                        );
                    }
                }
            }
        }
        return $Users;
    }

    public static function printRepOnColor($id_special, $period = "month", $format = "individual") {
        $Users = Report::getReportOnColor($id_special, $period, $format);
        if ($format == "individual") {
            foreach ($Users as $User) {
                echo $User['surname'] . "<br/>�������: " . $User['color']
                . ";<br/>�����-�����: " . $User['gray'] . ".<br/><br/>";
            }
        } else {
            foreach ($Users as $User) {
                echo $User['surname'];
                if (array_key_exists('color', $User)) {
                    echo "<table><tr><th>�������</th><th>����</th><th>�����</th><th>����</th><th>�����</th></tr>";
                    foreach ($User['color'] as $key => $doc) {
                        echo "<tr>";
                        UserPrint::printOneDeal(UserPrint::getDeal($doc['id_doc']));
                        echo "<td>" . $doc['count'] . "</td></tr>";
                    }
                    echo "</table><br/>";
                } else {
                    echo '<br/>�������� ������ �� ������ �� ��������� ������ <br/>';
                }
            }
        }
    }

    public static function shortReportOnSpecial($id_special, $date_start, $date_finish) {
        $Report = array();
        $q = "SELECT SUM(count) AS lists, COUNT(*) AS docs FROM tbl_list "
                . "WHERE stage = '$id_special' AND date_proc >= '$date_start' AND date_proc < '$date_finish'";
        $sql = mysql_query($q);
        $dp = mysql_fetch_object($sql);
        $Report = array(
            'lists' => $dp->lists,
            'docs' => $dp->docs
        );
        echo "������ - ", $id_special, "; �������: ", ($Report['lists'] ? $Report['lists'] : "0"), "; ���: ", $Report['docs'], ".";
    }

}

class ReportOutside {

    const MAX_SIZE_CLASTER = 665600; // ����� ������� (��)

    private static function isColor($numb) {
        return ($numb == 1 ? "�������� ������" : "�������");
    }

    private static function recClass(&$number, &$tmp, $doc_size) {
        if ($tmp + $doc_size < self::MAX_SIZE_CLASTER) {
            $tmp += $doc_size;
        } else {
            $tmp = $doc_size;
            $number++;
        }
    }

    private static function getDateScan($id_doc) {
        $q = "SELECT DATE(`date_proc`) as date_proc "
                . "FROM tbl_list WHERE stage = '1' AND id_doc = '$id_doc' ";
        $sql = mysql_query($q);
        return mysql_result($sql, 0);
    }

    // ��� �������� � ����������� ��� ���������� �� �������� ������ 
    public static function tcAct($date_start = "2013-01-01") {
        $i = 1; // ����� ������ ������
        $numb_klass = 1;
        $tmp = 0;
        $id_special = '2';
        $q = "SELECT l.count, DATE(l.date_proc) as date_proc, f.size_mc, f.size_wc, "
                . "f.cabinet, n.rubrika, n.fond, n.opis, n.delo "
                . "FROM tbl_list l, tbl_freedoc f, tbl_name n "
                . "WHERE l.stage = '$id_special' AND l.date_proc >= '$date_start' "
                . "AND l.id_doc = n.id_doc AND l.id_doc = f.id_doc";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql)) {
            $count = $dp->count;
            $date = $dp->date_proc;
            $color = $dp->cabinet;
            $fond = $dp->fond;
            $opis = $dp->opis;
            $delo = $dp->delo;
            $size_mc = $dp->size_mc;
            $size_wc = $dp->size_wc;
            self::recClass($numb_klass, $tmp, $size_mc);

            echo '<tr>
    <td>' . $i . '</td>
    <td>' . $fond . '</td>
    <td>' . $opis . '</td>
    <td>' . $delo . '</td>
    <td>' . $numb_klass . '</td> 
    <td>' . $date . '</td>
    <td>' . $count . '</td>
    <td>' . $size_mc . '</td>
    <td>' . $size_wc . '</td>
    <td>' . self::isColor($color) . '</td>';
            $i++;
        }
    }

    // ����� ����� ����� ����������� �� ����������� ��������� 
    public static function tcBC($date_start = "2013-01-01") {
        $i = 1; // ����� ������ ������
        $numb_klass = 1;
        $tmp = 0;
        $stage = '2';

        $q = "SELECT l.id_doc, l.count, DATE(l.date_proc) as date_proc, f.size_mc, "
                . "f.size_wc, f.cabinet, n.rubrika, n.fond, n.opis, n.delo "
                . "FROM tbl_list l, tbl_freedoc f, tbl_name n "
                . "WHERE l.stage = '$stage' AND l.date_proc >= '$date_start' "
                . "AND l.id_doc = n.id_doc AND l.id_doc = f.id_doc";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql)) {
            $count = $dp->count;
            $date = $dp->date_proc;
            $color = $dp->cabinet;
            $fond = $dp->fond;
            $opis = $dp->opis;
            $delo = $dp->delo;
            $size_mc = $dp->size_mc;
            $size_wc = $dp->size_wc;
            self::recClass($numb_klass, $tmp, $size_mc);

            echo '<tr>
    <td>' . $i . '</td>
    <td>' . $fond . '</td>
    <td>' . $opis . '</td>
    <td>' . $delo . '</td>
    <td>' . $numb_klass . '</td> 
    <td>' . self::getDateScan($dp->id_doc) . '</td>
    <td>' . $date . '</td>
    <td>' . $count . '</td>
    <td>' . $size_mc . '</td>
    <td>' . $size_wc . '</td>
    <td> - </td>';
            $i++;
        }
    }

    // ����� ����������� ������-����� �������� ���������� �� �������� ������
    public static function tcOpRegNmbOut($date_start = "2013-01-01", $storage_unit = 1) {
        /*
         * ������� �������� - ������� (storage_unit), ������� ����� - ���������
         */
        $tmp_size = 0;
        $tmp_deals = 0;
        $tmp_imgs = 0;
        $stage = '2';

        $q = "SELECT l.count, DATE(l.date_proc) as date_proc, f.size_mc, f.size_wc "
                . "FROM tbl_list l, tbl_freedoc f "
                . "WHERE l.stage = '$stage' AND l.date_proc >= '$date_start' AND l.id_doc = f.id_doc";
        $sql = mysql_query($q);
        while ($dp = mysql_fetch_object($sql)) {
            $size_mc = $dp->size_mc;

            if ($tmp_size + $size_mc < self::MAX_SIZE_CLASTER) {
                $tmp_size += $size_mc;
                $tmp_imgs += $dp->count;
                $tmp_deals++;
            } else {
                echo '<tr> 
    <td>' . $storage_unit . '</td>
    <td> 1 </td>
    <td>' . $tmp_imgs . '</td> 
    <td>' . $tmp_deals . '</td>
    <td>' . $tmp_size . '</td>
    <td>' . $dp->date_proc . '</td>
    <td> - </td>';
                $tmp_imgs = $dp->count;
                $tmp_deals = 1;
                $tmp_size = $size_mc;
                $storage_unit++;
            }
        }
        // ����� �������, ������������ �������
        echo '<tr> 
    <td>' . $storage_unit . '</td>
    <td> 1 </td>
    <td>' . $tmp_imgs . '</td> 
    <td>' . $tmp_deals . '</td>
    <td>' . $tmp_size . '</td>
    <td>' . date('Y-m-d') . '</td>
    <td> - </td>';
    }

}

?>